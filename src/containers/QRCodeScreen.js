import React, { useEffect } from 'react';
import Close from '../img/close.jpg';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux'

const QRCodeScreen = (props) => {

  let navigate = useNavigate();

  useEffect(() => {
    console.log('hmm',props.qrcode)
    window.scrollTo(0, 0);
  }, []);

  return (
    <div style={styles.container}>
      <img src={Close} style={styles.closelogo} onClick={()=> navigate("/home")}/>

    <div> Show the QR code below to the cashier. </div>
      <img src={props.qrcode} style={styles.logo} onClick={{}}/>
    </div>
  );
};

const styles={
  container:{
    display:'flex',
    justifyContent:'center',
    flexDirection:'column',
    alignItems:'center',
    height:'100vh'
  },
  logo:{
    width:250,
    alignItems:'center',
    justifyContent:'center',
    marginTop:'20%'
  },
  closelogo:{
    width:20,
    position:'absolute',
    top:20,
    right:20
  },
 
}

const mapStateToProps = state => {
  return {
      qrcode: state.home.qrcode
  }
} 

export default connect(mapStateToProps,null)(QRCodeScreen)
