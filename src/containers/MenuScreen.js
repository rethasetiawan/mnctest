import React, { useEffect,useState } from 'react';
import BottomTab from '../components/BottomTab';
import axios from 'axios';
import { connect } from 'react-redux'
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

const MenuScreen = (props) => {

    const [menu,setMenu] = useState([]);
    const [loading,setLoading] = useState(true);
    const [value, setValue] = React.useState('1');

    function numberWithDots(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      }

      useEffect(() => {
        async function getData(){
        const body = {
          show_all:1
        };
        const res = await axios.post('https://soal.staging.id/api/menu',body, {
          headers: { 'Authorization': `Bearer ${props.token}` },
        });
        console.log('res',res)
        setMenu(res.data.result)
        }

        if (menu.length == 0){
            getData();
          } else {
            setLoading(false)
          }
      });

    
    const subMenu = (data,index) =>{
        return(
            <div style={{display:'flex', flexDirection:'row',padding:10, borderBottom:'1px solid lightgray'}}>
                <img src={data.photo} style={{width:100}}/>
                <div style={{paddingRight:10}}>
                    <div style={{fontWeight:'bold'}}>{data.name}</div>
                    <div style={{fontSize:12}}>{data.description}</div>
                </div>
                <div style={{fontWeight:'bold'}}>{numberWithDots(data.price)}</div>
            </div>
        )
    }
    
    const mainMenu = (data,index) => {
        return(
            <div>
            <div style={styles.categoryName}>{data.category_name}</div>
            {data.menu.map((data, index) =>
                (
                subMenu(data,index) 
               ))} 
            </div>
        )
    }

    const reload = ()=>{
        window.location.reload();
    }

    
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
return (
    <div>
        <div style={styles.menu}>MENU</div>
    
        {loading ? null:
        <div>
            <TabContext value={value} >
                <TabList onChange={handleChange} 
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs ">
                {menu.categories.map((data, index) =>
                        (
                            <Tab label={data.category_name}   onChange={handleChange}   value={index} />
                        ))}
                </TabList>
            </TabContext>
        { menu.categories.map((data, index) =>
                (
                        mainMenu(data,index) 
                ))}
        </div>
       
        } 

        <BottomTab/>
    </div>
  )
};

const styles={
    menu:{
        fontWeight:'bold',
        fontSize:20,
        textAlign:'center',
        padding:10
    },
    categoryName:{
        backgroundColor:'#EEEEEE',
        padding:15,
        fontWeight:'bold'
    }
}

const mapStateToProps = state => {
    return {
        token: state.home.token,
     
    }
  }

export default connect(mapStateToProps,null)(MenuScreen)
