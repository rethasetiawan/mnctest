import React, { useEffect,useState } from 'react';
import Logo from '../img/logo.png';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import BottomTab from '../components/BottomTab';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux'
import { getQRCode,getToken } from '../redux/Actions/HomeActions';
import Modal from '@mui/material/Modal';
import Close from '../img/close.jpg';


import axios from 'axios';
const oauth = require('axios-oauth-client');


const HomeScreen = (props) => {
  const [token, setToken] = useState([]);
  const [dataProfil,setDataProfil] = useState([]);
  const [openModal,setOpenModal] = useState(false);

  const [loading,setLoading] = useState(true);

  function numberWithDots(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  let navigate = useNavigate();

  useEffect(() => {
    const email = localStorage.getItem("email");
    const password = localStorage.getItem("password");
    console.log(email, password)

    const getOwnerCredentials = oauth.client(axios.create(), {
      url: 'https://soal.staging.id/oauth/token',
      grant_type: 'password',
      client_id: 'e78869f77986684a',
      client_secret: '0a40f69db4e5fd2f4ac65a090f31b823',
      username: `${email}`,
      password: `${password}`,
      scope: '',
    });

    async function auth(){
      let response = await getOwnerCredentials();
      setToken(response)
      props.getToken(response.access_token)
    }

    if (token.length == 0){
      auth()
    }
  });

  useEffect(() => {
    async function getData(){
      const body = {
      };
    const res = await axios.get('https://soal.staging.id/api/home', {
      headers: { 'Authorization': `Bearer ${token.access_token}` },
    });
    setDataProfil(res.data.result)
    }

    if (dataProfil.length == 0){
      getData();
    } else {
      setLoading(false)
    }
    console.log('yes',dataProfil)
  });
  
  const Slideshow = (img,index) => {
    return(
      <div className="each-slide" key={index}>
        <div style={{...styles.backgroundContainer,
         backgroundImage: `url(${img})`
         }}>
        </div>
      </div>
    )
  }

  const nextQRCode = () =>{
   setOpenModal(true)
  }

  const reload = ()=>{
    window.location.reload();
}
  return (
    
    <div style={styles.container} >

      <Modal open={openModal}>
        <div style={styles.modalContainer}>
        <img src={Close} style={styles.closelogo} onClick={()=>setOpenModal(false)}/>

        <div> Show the QR code below to the cashier. </div>
          <img src={dataProfil.qrcode} style={styles.modalLogo} onClick={{}}/>
        </div>
      </Modal>

    <div style={{backgroundColor:'white'}}>
      <img src={Logo} style={styles.logo}/>
    </div>
    { 
      loading ? null : 
      <div>
        <div style={styles.cardContainer}>
          <div style={{margin:15}}>
          {dataProfil.greeting},
              <div style={styles.nameStyle}>
              {dataProfil.name}
              </div>

              <div style={styles.subContainer}>
                  <div onClick={nextQRCode} style={styles.logoContainer}>
                      <img src={dataProfil.qrcode} style={styles.codeLogo}/>
                  </div>

                  <div style={{...styles.subContainer,width:'70%',borderLeft:'2px dotted gray', paddingLeft:15}}>
                    <div>
                      <div>Saldo</div>
                      <div>Points</div>
                    </div>
                    <div>
                      <div style={{fontWeight:'bold'}}>Rp {numberWithDots(dataProfil.saldo)}</div>
                      <div style={{color:'green',display:'flex',justifyContent:'flex-end'}}>{numberWithDots(dataProfil.point)}</div>
                    </div>
                  </div>
              </div>

          </div>

        </div>
        
        <Slide easing="ease" duration={2000} indicators arrows={false}>
            {dataProfil.banner.map((slideImage, index)=> (
                Slideshow(slideImage,index)
                ))} 
        </Slide>
      </div>
    }
        <div style={styles.sticky}>
          <BottomTab/>
        </div>
        <button onClick={()=>reload()}>refresh</button>

    </div>
  );
};

const styles={
  container:{
    display:'flex',
    flexDirection:'column',
    background:'#EEEEEE',
    height:'100vh'
  },
  logo:{
    width:150,
  },
  codeLogo:{
    width:35,
  },
  logoContainer:{
    height:60,
    width:60,
    borderRadius:100,
    boxShadow: "3px 2px 10px #9E9E9E",
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    marginTop:10
  },
  subContainer:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  },
  nameStyle:{
      fontWeight:'bold',
      marginTop:10,
      marginBottom:10
  },
  saldoContainer:{
    display:'flex',
    justifyContent:'space-between',
    backgroundColor:'gray',
  },
   cardContainer:{
    width:'80%',
    backgroundColor:'white',
    borderRadius:10,
    boxShadow: "1px 3px 1px #9E9E9E",
    padding:10,
    // marginRight:'auto',
    // marginLeft:'auto'
    margin:'20px auto 30px auto'
   },
   backgroundContainer:{
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height:200,
  },
  sticky:{
    // padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "65px",
    width: 412,
  },
  modalContainer:{
    display:'flex',
    justifyContent:'center',
    flexDirection:'column',
    alignItems:'center',
    height:'100vh',
    background:'white'
  },
  modalLogo:{
    width:250,
    alignItems:'center',
    justifyContent:'center',
    marginTop:'20%'
  },
  closelogo:{
    width:20,
    position:'absolute',
    top:20,
    right:20
  },

}

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = {
  getQRCode,
  getToken
};

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen)