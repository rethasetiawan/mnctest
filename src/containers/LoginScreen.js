import React, { useEffect } from 'react';
import Logo from '../img/logo.png';
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom';

const LoginScreen = (props) => {
  let navigate = useNavigate();
  const handleChangeEmail= (event) => {
    localStorage.setItem("email", event.target.value)
  };

  const handleChangePassword= (event) => {
    localStorage.setItem("password", event.target.value)
  };

  const handleLogin = () =>{
    console.log(props.password, props.email)
    navigate("/home")
  }

  return (
    <div style={styles.container}>
      <img src={Logo} style={styles.logo}/>
        <div>Email</div>
        <input
         style={styles.textBox}
         onChange={(event) => handleChangeEmail(event)}
        />

        <div>Password</div>
        <input 
          style={styles.textBox}
          onChange={(event) => handleChangePassword(event)}
        />
      <button style={styles.button} onClick={()=>handleLogin()}>Login</button>
    </div>
  );
};

const styles={
  container:{
    display:'flex',
    justifyContent:'center',
    flexDirection:'column',
    alignItems:'center',
    background:'#EEEEEE',
    height:'100vh'
  },
  logo:{
    width:300,
    alignItems:'center',
    justifyContent:'center',
    marginBottom:'25%'
  },
  textBox:{
    border:'1px solid gray',
    background:'white',
    borderRadius:5,
    width:'60%',
    height:30,
    margin:10
  },
  button:{
      border:'1px solid gray',
      padding:10,
      margin:20,
      width:150,
      backgroundColor:'white',
      borderRadius:10,
      fontWeight:'bold'
  }
}

export default LoginScreen

