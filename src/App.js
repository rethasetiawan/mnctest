import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// import MainPage from './containers/MainPage';
import LoginScreen from './containers/LoginScreen';
import HomeScreen from './containers/HomeScreen';
import QRCodeScreen  from './containers/QRCodeScreen';
import MenuScreen  from './containers/MenuScreen';
import BottomTab  from './components/BottomTab';


function App() {
  return (
    <div>
    <Router >
      <Routes>
        <Route path="/" element={ <LoginScreen/>} />
        <Route path="/home" element={ <HomeScreen/>} />
        <Route path="/barcode" element={ <QRCodeScreen/>} />
        <Route path="/menu" element={ <MenuScreen/>} />
        {/* <Route path="/selected" element={ <SelectedPage/>} /> */}
      </Routes>
    </Router>
  </div>
  );
}

export default App;
