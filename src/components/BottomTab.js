import React from 'react';
import Home from '../img/home.png';
import Menu from '../img/menu.png';
import { useNavigate } from 'react-router-dom';

const BottomTab = (props) => {
    let navigate = useNavigate();

	return (
    
        <div style ={styles.container}>
                <div onClick={()=> navigate("/home")}>
                    <img src={Home} style={styles.icon}/>
                    <div>Home</div>
                </div>

                <div onClick={()=> navigate("/menu")}>
                    <img src={Menu} style={styles.icon}/>
                    <div>Menu</div>
                </div>
        </div>

  )
};

const styles = {
    container:{
        background:'white',
        // position:'absolute',
        // bottom:0,
        width:'100%',
        flexDirection:'row',
        display:'flex',
        justifyContent:'space-around',
        boxShadow: "3px 2px 10px #9E9E9E",
    },
    icon:{
        width:40
    }
}
export default BottomTab;