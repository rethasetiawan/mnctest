import { 
  GET_QRCODE,
  GET_TOKEN,

 } from '../types';

const getQRCode = (qrcode) => ({
  type: GET_QRCODE,
  payload: qrcode,
});
const getToken = (token) => ({
  type: GET_TOKEN,
  payload: token,
});




export { getQRCode,getToken };
