import { combineReducers } from 'redux';

import HomeReducer from './HomeReducers';


const rootReducer = combineReducers({
  home: HomeReducer,
});

export default rootReducer;
