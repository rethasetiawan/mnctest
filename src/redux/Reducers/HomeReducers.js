import {
 GET_QRCODE,
 GET_TOKEN,

 } 
  from '../types';
  
  const INITIAL_STATE = {
    qrcode:'',
    token:'',
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
     
      case GET_QRCODE:
          return {
            ...state,
            qrcode: action.payload,
          };
      case GET_TOKEN:
          return {
            ...state,
            token: action.payload,
          };
    
      default:
        return state;
    }
  };
  